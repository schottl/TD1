<?php

namespace TheFeed\Controleur;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use TheFeed\Lib\Conteneur;
use TheFeed\Lib\MessageFlash;
use Twig\Environment;
use Symfony\Component\DependencyInjection\ContainerInterface;


class ControleurGenerique {

    public function __construct(private ContainerInterface $container)
    {}

    protected  function afficherVue(string $cheminVue, array $parametres = []): RedirectResponse
    {
        extract($parametres);
        $messagesFlash = MessageFlash::lireTousMessages();
        ob_start();
        require __DIR__ . "/../vue/$cheminVue";
        $corpsReponse = ob_get_clean();
        return new RedirectResponse($corpsReponse);
    }

    // https://stackoverflow.com/questions/768431/how-do-i-make-a-redirect-in-php
    protected  function rediriger(string $route, array $param = []) : RedirectResponse
    {
        $generateurUrl = Conteneur::recupererService("generateurUrl");
        $url = $generateurUrl->generate($route,$param);
//        header("Location: ". $url);
        return new RedirectResponse($url);
    }

    public  function afficherErreur($messageErreur = "", $statusCode = 400): Response
    {
        $reponse = $this->afficherTwig("erreur.html.twig",["messageErreur" => $messageErreur]);

        $reponse->setStatusCode($statusCode);
        return $reponse;
    }

    protected  function afficherTwig(string $cheminVue, array $parametres = []): Response
    {
        /** @var Environment $twig */
        $twig = Conteneur::recupererService("twig");
        $corpsReponse = $twig->render($cheminVue, $parametres);
        return new Response($corpsReponse);
    }


}