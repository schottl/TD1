<?php

namespace TheFeed\Controleur;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Lib\MessageFlash;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationService;
use TheFeed\Service\UtilisateurService;

class ControleurUtilisateur extends ControleurGenerique
{
    private $publicationService;
    private $utilisateurService;

    public function __construct(PublicationService $publicationService,UtilisateurService $utilisateurService)
    {
        $this->publicationService = $publicationService;
        $this->utilisateurService = $utilisateurService;
    }

    public function afficherErreur($messageErreur = "", $controleur = ""): Response
    {
       return parent::afficherErreur($messageErreur, "utilisateur");
    }

    /**
     * @throws ServiceException
     */
    #[Route(path: '/utilisateurs/{idUtilisateur}/publications', name:'afficherPublications', methods:["GET"])]
    public  function afficherPublications($idUtilisateur): Response
    {
        $utilisateur =   $this->utilisateurService->recupererUtilisateurParId($idUtilisateur);
        $publications = $this->publicationService->recupererPublicationsUtilisateur($idUtilisateur);
        return $this->afficherTwig("publication/page_perso.html.twig", [
            "publications" => $publications,
            "login"=>$utilisateur->getLogin()
        ]);
        }

    #[Route(path: '/inscription', name:'inscriptionGET', methods:["GET"])]
    public  function afficherFormulaireCreation(): Response
    {
        return $this->afficherTwig("utilisateur/inscription.html.twig");
    }

    #[Route(path: '/inscription', name:'inscriptionPOST', methods:["POST"])]
    public  function creerDepuisFormulaire(): Response
    {
        var_dump($_POST);
        $login = $_POST['login'];
        $motDePasse = $_POST['mot-de-passe'];
        $adresseMail = $_POST['email'];
        $nomPhotoDeProfil = $_FILES['nom-photo-de-profil'];
        try {
              $this->utilisateurService->creerUtilisateur($login,$motDePasse,$adresseMail,$nomPhotoDeProfil);
        }
        catch(ServiceException $e) {
            MessageFlash::ajouter('error',"erreur lors de la création");
            return $this->rediriger('afficherFormulaireCreation');
        }
        MessageFlash::ajouter('success',"L'utilisateur a bien été créé !");
        return $this->rediriger('afficherListe');
    }

    #[Route(path: '/connexion', name:'afficherFormulaireConnexion', methods:["GET"])]
    public  function afficherFormulaireConnexion(): Response
    {
        return $this->afficherTwig("utilisateur/connexion.html.twig");
    }

    #[Route(path: '/connexion', name:'connecter', methods:["POST"])]
    public  function connecter(): RedirectResponse
    {
        try {
            $this->utilisateurService->connecter();
        }
        catch(ServiceException $e) {
            MessageFlash::ajouter('error',"erreur lors de la connexion");
            return $this->rediriger('afficherFormulaireConnexion');
        }
        return $this->rediriger("afficherListe");
    }

    #[Route(path: '/deconnexion', name:'deconnecter', methods:["GET"])]
    public  function deconnecter(): RedirectResponse
    {
        try {
            $this->utilisateurService->deconnecter();
        }
        catch(ServiceException $e) {
            MessageFlash::ajouter('error',"erreur lors de la déconnexion");
        }
        return $this->rediriger("afficherListe");
    }




}
