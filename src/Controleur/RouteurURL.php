<?php
namespace TheFeed\Controleur;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ContainerControllerResolver;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Loader\AttributeDirectoryLoader;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use TheFeed\Configuration\ConfigurationBDDMySQL;
use TheFeed\Lib\AttributeRouteControllerLoader;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\Conteneur;
use TheFeed\Lib\MessageFlash;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFunction;


class RouteurURL
{

    /**
     * @throws \Exception
     */
    public static function traiterRequete(): void
    {

        $conteneur = new ContainerBuilder();
        $conteneur->setParameter('project_root', __DIR__.'/../..');
        $conteneur->register('configuration_bdd_my_sql', ConfigurationBDDMySQL::class);


        //On indique au FileLocator de chercher à partir du dossier de configuration
        $loader = new YamlFileLoader($conteneur, new FileLocator(__DIR__."/../Configuration"));
        //On remplit le conteneur avec les données fournies dans le fichier de configuration
        $loader->load("conteneur.yml");


        $requete = Request::createFromGlobals();

        $fileLocator = new FileLocator(__DIR__);
        $attrClassLoader = new AttributeRouteControllerLoader();
        $routes = (new AttributeDirectoryLoader($fileLocator, $attrClassLoader))->load(__DIR__);



        $contexteRequete = (new RequestContext())->fromRequest($requete);

        //Après l'instanciation de l'objet $contexteRequete
        $conteneur->set('request_context', $contexteRequete);
        //Après que les routes soient récupérées
        $conteneur->set('routes', $routes);

        $generateurUrl=$conteneur->get('url_generator');
        $assistantUrl=$conteneur->get('url_helper');
        $twig=$conteneur->get('twig');

        $twig->addFunction(new TwigFunction("route", $generateurUrl->generate(...)));
        $twig->addFunction(new TwigFunction("asset",  $assistantUrl->getAbsoluteUrl(...)));

        $twig->addGlobal('idUser', ConnexionUtilisateur::getIdUtilisateurConnecte());
        $twig->addGlobal('messagesFlash', new MessageFlash());

        try {
            $associateurUrl = new UrlMatcher($routes, $contexteRequete);
            $donneesRoute = $associateurUrl->match($requete->getPathInfo());
            $requete->attributes->add($donneesRoute);

            $resolveurDeControleur = new ContainerControllerResolver($conteneur);
            $controleur = $resolveurDeControleur->getController($requete);

            $resolveurDArguments = new ArgumentResolver();
            $arguments = $resolveurDArguments->getArguments($requete, $controleur);

            $reponse = call_user_func_array($controleur, $arguments);
        } catch (ResourceNotFoundException  $exception) {
            $reponse = (new ControleurGenerique())->afficherErreur($exception->getMessage(), 404);
        } catch (MethodNotAllowedException $exception) {
            $reponse = (new ControleurGenerique())->afficherErreur($exception->getMessage(), 405);
        } catch (\Exception $exception) {
            $reponse = (new ControleurGenerique())->afficherErreur($exception->getMessage()) ;
        }
        $reponse->send();
    }




}
