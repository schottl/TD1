<?php

namespace TheFeed\Test;

use PhpParser\Node\Expr\Array_;
use PHPUnit\Framework\TestCase;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationService;

class PublicationServiceTest  extends TestCase
{

    private $service;

    protected function setUp(): void
    {
        parent::setUp();
        $this->service = new PublicationService();
    }

    public function testCreerPublicationUtilisateurInexistant()
    {
        $idUtilisateurInexistant = -1;

        $this -> expectException(ServiceException::class);
        $this->service->creerPublication($idUtilisateurInexistant, "big test");
    }

    public function testCreerPublicationVide()
    {
        $idUtilisateurInexistant = 1;

        $this -> expectException(ServiceException::class);
        $this->service->creerPublication($idUtilisateurInexistant,"");
    }

    public function testCreerPublicationTropGrande()
    {
        $idUtilisateurInexistant = 1;

        $this -> expectException(ServiceException::class);
        $this->service->creerPublication($idUtilisateurInexistant,str_repeat('a',255));
    }

    public function testNombrePublications()
    {
       $nbPublications = 13;
       $publications= count($this->service->recupererPublications());
       $this->assertEquals($nbPublications,$publications);
    }

    public function testNombrePublicationsUtilisateur()
    {
       $nbPublications = 3;
       $publications= count($this->service->recupererPublicationsUtilisateur(1));
       $this->assertEquals($nbPublications,$publications);
    }

    public function testNombrePublicationsUtilisateurInexistant()
    {
   $nbPublications = 0;
       $publications= count($this->service->recupererPublicationsUtilisateur(-1));
       $this->assertEquals($nbPublications,$publications);
    }






}