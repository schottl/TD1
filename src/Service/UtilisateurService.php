<?php

namespace TheFeed\Service;

use Symfony\Component\HttpFoundation\Response;
use TheFeed\Controleur\ControleurUtilisateur;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Service\Exception\ServiceException;
use function PHPUnit\Framework\logicalOr;

class UtilisateurService
{

    private $utilisateurRepository;

    public function __construct(UtilisateurRepository $publicationRepository)
    {
        $this->utilisateurRepository = $publicationRepository;
    }


    /**
     * @throws ServiceException
     */
    public function creerUtilisateur($login, $motDePasse, $email, $donneesPhotoDeProfil): void
    {
        if ($login == null || $motDePasse == null || $email == null || $donneesPhotoDeProfil == null) {
            throw new ServiceException('error', "Une donnée du formulaire est null");
        } else {
            if (strlen($login) < 4 || strlen($login) > 20) {
                throw new ServiceException('error', "Le login doit contenir au moins 3 caractères");
            }
            if (strlen($motDePasse) < 8 || !preg_match("#^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,20}$#", $motDePasse)) {
                throw new ServiceException('error', "Le mot de passe doit contenir au moins 8 caractères et inclure des lettres et des chiffres");
            }
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                throw new ServiceException('error', "L'adresse email n'est pas valide");
            }
            if ($this->utilisateurRepository->recupererParLogin($login) != null) {
                throw new ServiceException('error', "Cet utilisateur existe déjà");
            }
            if ($this->utilisateurRepository->recupererParEmail($email) != null) {
                throw new ServiceException('error', "Cette adresse mail est déjà prise");
            }
            $explosion = explode('.', $donneesPhotoDeProfil['name']);
            $fileExtension = end($explosion);
            if (!in_array($fileExtension, ['png', 'jpg', 'jpeg'])) {
                throw new ServiceException('error', "Extension de l'image pas supportée");
            }
            $pictureName = uniqid() . '.' . $fileExtension;
            $from = $donneesPhotoDeProfil['tmp_name'];
            $to = __DIR__ . "/../../ressources/img/utilisateurs/$pictureName";
            move_uploaded_file($from, $to);

            $motDePasseChiffre = MotDePasse::hacher($motDePasse);

            $utilisateur = Utilisateur::create($login, $motDePasseChiffre, $email, $pictureName);
            $this->utilisateurRepository->ajouter($utilisateur);
        }
    }


    public function recupererUtilisateurParId($idUtilisateur, $autoriserNull = true): ?Utilisateur
    {
        $utilisateur = $this->utilisateurRepository->recupererParClePrimaire($idUtilisateur);
        if (!$autoriserNull && is_null($utilisateur)) {
            throw new ServiceException('Login inconnu');
        }
        return $utilisateur;
    }


    public function connecter(): void
    {
        if (!(isset($_POST['login']) && isset($_POST['mot-de-passe']))) {
            throw new ServiceException("error", "Login ou mot de passe manquant.");
        }
        $utilisateurRepository  =$this->utilisateurRepository;
        /** @var Utilisateur $utilisateur */
        $utilisateur = $utilisateurRepository->recupererParLogin($_POST["login"]);

        if ($utilisateur == null) {
            throw new ServiceException("error", "Login inconnu.");
        }

        if (!MotDePasse::verifier($_POST["mot-de-passe"], $utilisateur->getMdpHache())) {
            throw new ServiceException("error", "Mot de passe incorrect.");
        }
        ConnexionUtilisateur::connecter($utilisateur->getIdUtilisateur());
        MessageFlash::ajouter("success", "Connexion effectuée.");
    }

    public function deconnecter(): void
    {
        if (!ConnexionUtilisateur::estConnecte()) {
            throw new ServiceException("error", "Utilisateur non connecté.");
        }
        ConnexionUtilisateur::deconnecter();
        MessageFlash::ajouter("success", "L'utilisateur a bien été déconnecté.");
    }




}