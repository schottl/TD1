<?php
namespace TheFeed\Service;

use TheFeed\Controleur\ControleurUtilisateur;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Service\Exception\ServiceException;

class PublicationService
 {

    private $publicationRepository;
    private $utilisateurRepository;

    public function __construct(PublicationRepository $publicationRepository,UtilisateurRepository $utilisateurRepository)
    {
        $this->publicationRepository = $publicationRepository;
        $this->utilisateurRepository = $utilisateurRepository;
    }


    public function recupererPublications(): array{
         return $this->publicationRepository->recuperer();
     }

     public function creerPublication($idUtilisateur, $message) : void {
         $utilisateur = $this->utilisateurRepository->recupererParClePrimaire($idUtilisateur);
         if ($utilisateur == null) {
             throw new ServiceException("Il faut être connecté pour publier un feed");
//             return ControleurPublication::rediriger('connecter');
         }
         if ($message == null || $message == "") {
             throw new ServiceException("Le message ne peut pas être vide!");
//             return ControleurPublication::rediriger('afficherListe');
         }
         if (strlen($message) > 250) {
             throw new ServiceException("Le message ne peut pas dépasser 250 caractères!");
//             return ControleurPublication::rediriger('afficherListe');
         }
         $publication = Publication::create($message, $utilisateur);
         $this->publicationRepository->ajouter($publication);
     }


    public function recupererPublicationsUtilisateur($idUtilisateur): array
    {
        return $this->publicationRepository->recupererParAuteur($idUtilisateur);
    }

}